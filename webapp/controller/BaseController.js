sap.ui.define(
    [
        "sap/ui/core/mvc/Controller",
        "sap/ui/core/routing/History",
        "sap/ui/core/UIComponent",
        "zmm_vendor_po_cr/model/formatter",
        "zmm_vendor_po_cr/model/models",
        "sap/ui/model/Filter",
        "sap/ui/model/Sorter",
        "sap/ui/model/FilterOperator",
        "sap/m/MessageBox",
        "sap/ui/export/Spreadsheet",
        'sap/ui/export/library',
        'sap/m/MessageToast'
    ],
    function (Controller, History, UIComponent, formatter, models, Filter, Sorter, FilterOperator, MessageBox, Spreadsheet, exportLibrary, MessageToast) {
        "use strict";
        var EdmType = exportLibrary.EdmType;
        return Controller.extend("zmm_vendor_po_cr.controller.BaseController", {
            formatter: formatter,

            /**
             * Convenience method for getting the view model by name in every controller of the application.
             * @public
             * @param {string} sName the model name
             * @returns {sap.ui.model.Model} the model instance
             */
            getModel: function (sName) {
                return this.getView().getModel(sName);
            },
            onOpenDialog: function (oEvent, sDialogName) {

                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment("zmm_vendor_po_cr.view.popovers." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                this[sDialogName].open();
            },
            onOpenVizFrameDialog: function (oEvent, sDialogName) {

                if (!this[sDialogName]) {
                    this[sDialogName] = sap.ui.xmlfragment('vizFrameDialog', "zmm_vendor_po_cr.view.popovers." + sDialogName + 'Dialog', this);
                    this.getView().addDependent(this[sDialogName]);
                }
                var _graphDialog = sap.ui.core.Fragment.byId("vizFrameDialog", "idVizFrameDialog");
                oComponent_VendCr._detailController.createFrameDialog(_graphDialog);
                this[sDialogName].open();
            },
            onCloseDialog: function (oEvent, sDialogName) {

                try {
                    this[sDialogName].close();
                } catch (e) {
                    oComponent_VendCr[sDialogName].close();
                }

            },
            onOpenPopover: function (oEvent, sName) {
                var oRow = oEvent.getSource().getBindingContext("JSON").getObject();
                oComponent_VendCr.getModel("JSON").setProperty("/SelectedPopover", oRow);
                if (!this[sName]) {
                    this[sName] = sap.ui.xmlfragment("zmm_vendor_po_cr.view.popovers." + sName + 'Popover', this);
                    this.getView().addDependent(this[sName]);
                }
                this[sName].openBy(oEvent.getSource());
            },
            NavInCart: function (evt) {
                var navCon = sap.ui.getCore().byId("navCon");
                navCon.to("page2", "Slide");
            },
            SaveAndSand: function (oEvent) {
                this.onCloseDialog(oEvent, 'Cart');
                this.onOpenDialog(oEvent, 'ApproveOrder');
            },
            onValueHelpClose: function (oEvent, sType) {
                var oSelectedItem = oEvent.getParameter("selectedItem");

                if (!oSelectedItem) {
                    return;
                }
                else {
                    var sKey = oSelectedItem.getProperty("description"),
                        sDesc = oSelectedItem.getTitle();
                    if (sType === 'Vendor') {
                        var sKey = oSelectedItem.getProperty("description");
                        oComponent_VendCr.getModel("JSON").setProperty("/Filters/Vendor", sKey);
                        oComponent_VendCr.getModel("JSON").setProperty("/Filters/VendorName", sDesc);
                    }
                    else if (sType === 'Kostl') {
                        var sKey = oSelectedItem.getProperty("description");
                        oComponent_VendCr.getModel("JSON").setProperty("/Filters/Kostl", sKey);
                        oComponent_VendCr.getModel("JSON").setProperty("/Filters/KostlName", sDesc);
                    }

                }
                oEvent.getSource().getBinding("items").filter([]);


            },

            /**
             * Convenience method for setting the view model in every controller of the application.
             * @public
             * @param {sap.ui.model.Model} oModel the model instance
             * @param {string} sName the model name
             * @returns {sap.ui.mvc.View} the view instance
             */
            setModel: function (oModel, sName) {
                return this.getView().setModel(oModel, sName);
            },
            onPressEditCart: function (bEdit) {
                oComponent_VendCr.getModel("JSON").setProperty("/editCart", !bEdit);
            },
            onPressDeleteItem: function (oEvent) {
                var oSelectedItem = oEvent.getSource().getBindingContext("JSON").getObject();
                var oPath = oEvent.getSource().getBindingContext("JSON").getPath();
                oComponent_VendCr.getModel("JSON").setProperty("/deleteItem", oSelectedItem);
                oComponent_VendCr.getModel("JSON").setProperty("/deleteItemPath", oPath);
                this.onOpenDialog('', 'DeleteItem')
            },
            onCancelDeleteItem: function (oEvent) {
                oComponent_VendCr.getModel("JSON").setProperty("/deleteItem", '');
                this.onCloseDialog('', 'DeleteItem');
            },
            onDeleteItem: function (oEvent) {
                var model = oComponent_VendCr.getModel("JSON");
                var modelData = model.getData();
                var cartItems = modelData.CartItems;
                let aPath = model.getProperty("/deleteItemPath").split("/");
                let indexToDelete = aPath[aPath.length - 1];
                var deletedItems = modelData.deletedItems;
                deletedItems.push(modelData.deleteItem);
                cartItems.splice(indexToDelete, 1);
                modelData.cartTotalPrice = this.onSumTableChanged(modelData.CartItems);
                model.refresh(true);
                this.onCloseDialog('', 'DeleteItem');
            },
            onItemCountChange: function (oEvent) {
                var oRow = oEvent.getSource().getBindingContext("JSON").getObject();
                oRow.OrderQuan = oRow.OrderQuan.replace(/[^0-9/.]/g, '');
                oRow.OrderQun = Number(oRow.OrderQuan) === 0 ? '' : parseInt(oRow.OrderQuan);
                oRow.ToltalPrice = (parseInt(Number(oRow.OrderQuan)) * Number(oRow.NetPrice)).toFixed(2);
                oComponent_VendCr.getModel("JSON").refresh();
                var items = oComponent_VendCr.getModel("JSON").getProperty("/CartItems");
                var TotalPrice = 0;
                for (var i = 0; i < items.length; i++) {
                    TotalPrice += parseInt(Number(items[i].OrderQuan)) * Number(items[i].NetPrice);
                }
                oComponent_VendCr.getModel("JSON").setProperty("/cartTotalPrice", TotalPrice.toFixed(2));
            },
            cartDialogClose: function (oEvent) {
                var navCon = sap.ui.getCore().byId("navCon");
                navCon.to("page1", "Slide");
                oComponent_VendCr.getModel("JSON").setProperty("/cartTotalPrice", '');
                oComponent_VendCr.getModel("JSON").setProperty("/CartItems", '');
                oComponent_VendCr.getModel("JSON").setProperty("/editCart", false);
                oComponent_VendCr.getModel("JSON").setProperty("/newOrder", {
                    PriceOutput: true,
                    LgortFlag: false
                });
            },
            onCancelEditCart: function (oEvent) {
                var aOldItems = oComponent_VendCr.getModel("JSON").getProperty("/oldCartItems");
                var sCartTotalPrice = oComponent_VendCr.getModel("JSON").getProperty("/oldCartTotalPrice");
                oComponent_VendCr.getModel("JSON").setProperty("/CartItems", jQuery.extend(true, [], aOldItems));
                oComponent_VendCr.getModel("JSON").setProperty("/cartTotalPrice", sCartTotalPrice);
                oComponent_VendCr.getModel("JSON").setProperty("/editCart", false);
            },
            onSaveChangeCart: function (oEvent) {
                var aItems = oComponent_VendCr.getModel("JSON").getProperty("/CartItems");
                var aDeletedItems = oComponent_VendCr.getModel("JSON").getProperty("/deletedItems");
                var sCartTotalPrice = oComponent_VendCr.getModel("JSON").getProperty("/cartTotalPrice");
                for (var i = 0; i < aItems.length; i++) {
                    if (!aItems[i].Valid) {
                        MessageToast.show(oComponent_VendCr.i18n('SearchPageErrorMsg2'), {
                            duration: 1500
                        });
                        return;
                    }
                }
                oComponent_VendCr.getModel("JSON").setProperty("/oldCartItems", jQuery.extend(true, [], aItems));
                oComponent_VendCr.getModel("JSON").setProperty("/oldCartTotalPrice", sCartTotalPrice);
                oComponent_VendCr.getModel("JSON").setProperty("/editCart", false);
                var sTableItems = oComponent_VendCr.getModel("JSON").getProperty("/items");
                for (var i = 0; i < aItems.length; i++) {
                    for (var j = 0; j < sTableItems.length; j++) {
                        if (aItems[i].Matnr === sTableItems[j].Matnr) {
                            sTableItems[j].OrderQuan = aItems[i].OrderQuan;
                            sTableItems[j].ToltalPrice = aItems[i].ToltalPrice;
                            break;
                        }
                    }
                }
                for (var i = 0; i < aDeletedItems.length; i++) {
                    for (var j = 0; j < sTableItems.length; j++) {
                        if (aDeletedItems[i].Matnr === sTableItems[j].Matnr) {
                            sTableItems[j].Selected = false;
                            break;
                        }
                    }
                }
                oComponent_VendCr.getModel("JSON").refresh(true);
                oComponent_VendCr.getModel("JSON").setProperty("/TotalPrice", sCartTotalPrice);
                oComponent_VendCr.getModel("JSON").setProperty("/CartCount", aItems.length);
            },
            onSumTableChanged: function (items) {
                var TotalPrice = 0;
                for (var i = 0; i < items.length; i++) {
                    if (items[i].Selected) {
                        TotalPrice += parseInt(Number(items[i].OrderQuan)) * Number(items[i].NetPrice);
                    }
                }
                return TotalPrice;
            },
            /**
             * Convenience method for getting the resource bundle.
             * @public
             * @returns {sap.ui.model.resource.ResourceModel} the resourceModel of the component
             */
            getResourceBundle: function () {
                return this.getOwnerComponent().getModel("i18n").getResourceBundle();
            },

            /**
             * Method for navigation to specific view
             * @public
             * @param {string} psTarget Parameter containing the string for the target navigation
             * @param {Object.<string, string>} pmParameters? Parameters for navigation
             * @param {boolean} pbReplace? Defines if the hash should be replaced (no browser history entry) or set (browser history entry)
             */
            navTo: function (oEvent, sPage) {
                this.getRouter().navTo(sPage);
            },
            onMantrtokenUpdate: function (oEvent) {
                var matnr = oComponent_VendCr._searchController.getView().byId("MatnrInput");
                matnr.setValueState("None");
            },
            beforeCloseShDialog(oEvent, sDialogName) {
                // reset the filter
                var oBinding = oEvent.getSource().getBinding("items");
                oBinding.filter([]);
                this.onCloseDialog(oEvent, sDialogName);
            },
            onConfirm: function (oEvent, sType) {
                // reset the filter
                var oBinding = oEvent.getSource().getBinding("items");
                oBinding.filter([]);
                const aSelected = oEvent.getParameter("selectedContexts");
                const model = oComponent_VendCr.getModel("JSON");
                var aTokens = this.CreateToken(aSelected, sType);
                model.setProperty("/Filters/" + sType, aTokens);
                if(sType !== 'Matnrs'){
                    oComponent_VendCr._MasterController.filterItems();
                }
                
                this.onCloseDialog('', sType + 'Sh');
                // this.closeDialog(oEvent);
            },
            searchSh: function (oEvent, sType, sDesc) {
                const value = oEvent.getParameter("value");
                const oFilter = new Filter({
                    filters: [new Filter(sType, FilterOperator.Contains, value),
                    new Filter(sType, FilterOperator.Contains, value.toUpperCase()),
                    new Filter(sDesc, FilterOperator.Contains, value),
                    new Filter(sDesc, FilterOperator.Contains, value.toUpperCase())
                    ],
                    and: false
                });

                oEvent.getSource().getBinding("items").filter([oFilter]);
            },
            CreateToken: function (aSelected, sKey, sModel) {
                var aTokens = [];
                for (var i = 0; i < aSelected.length; i++) {
                    var object = aSelected[i].getObject();
                    switch (sKey) {
                        case 'Matnr':
                            aTokens.push({
                                Matnr: object[sKey]
                            });
                            break;
                        case 'AtcCode':
                            aTokens.push({
                                AtcCode: object[sKey]
                            });
                            break;
                        case 'PurchasingGroup':
                            aTokens.push({
                                PurchasingGroup: object[sKey]
                            });
                            break;
                        case 'Matnrs':
                            var matnr = oComponent_VendCr._searchController.getView().byId("MatnrInput");
                            matnr.setValueState("None");
                            aTokens.push({
                                vendor: object['VendorNumber'],
                                vendorName: object['VendorDescription'],
                                text: object['MatNumber'],
                                Matnrs: object['MatNumber']
                            });
                    }


                }
                return aTokens;

            },
            onDeleteToken: function (oEvent, sType) {
                const sKey = oEvent.getSource().getKey();
                const model = oComponent_VendCr.getModel("JSON");
                var aTokens = model.getProperty("/Filters/" + sType);
                const index = this.findIndexToDelete(aTokens, sKey, sType);
                aTokens.splice(index, 1);
                model.setProperty("/Filters/" + sType, aTokens);
                if (sType !== 'Matnrs') {
                    oComponent_VendCr._MasterController.filterItems();
                }
                // this._filter();
            },
            findIndexToDelete: function (aTokens, sKey, sType) {
                for (var i = 0; i < aTokens.length; i++) {
                    if (sKey === aTokens[i][sType]) {
                        return i;
                    }
                }
            },
            getRouter: function () {
                return UIComponent.getRouterFor(this);
            },
            onChangeDate: function (oEvent) {
                var bValid = oEvent.getParameter("valid");
                var oEventSource = oEvent.getSource();
                var beginDate = oComponent_VendCr._searchController.getView().byId("SearchDateFrom"),
                    endDate = oComponent_VendCr._searchController.getView().byId("SearchDateEnd");
                if (beginDate.getDateValue() <= endDate.getDateValue() && endDate.isValidValue()) {
                    endDate.setValueState("None");
                }
                oEvent.getSource().setValueState(bValid ? "None" : "Error");


                // var sValue = oEvent.getParameter("newValue");
            },
            onChangeDeliveryDraftDate: function(oEvent){
                debugger;
                var bValid = oEvent.getParameter("valid");
                oComponent_VendCr.getModel("JSON").setProperty("/DraftFilters/validDate" , bValid);
            },
            onChangeCartDate: function (oEvent) {
                var bValid = oEvent.getParameter("valid");
                var Object = oEvent.getSource().getBindingContext("JSON").getObject();
                oEvent.getSource().setValueState(bValid ? "None" : "Error");
                Object.Valid = bValid;

                // var sValue = oEvent.getParameter("newValue");
            },
            enterOrderCount: function (oEvent, sType) {
                var oModelData = oComponent_VendCr.getModel("JSON").getData(),
                    allItems = sType === 'genric' ? oModelData.OrderGenerics : oModelData.items,
                    TotalPrice = 0,
                    oRow = oEvent.getSource().getBindingContext("JSON").getObject();
                oRow.OrderQuan = oRow.OrderQuan.replace(/[^0-9/.]/g, '');
                oRow.OrderQun = Number(oRow.OrderQuan) === 0 ? '' : parseInt(oRow.OrderQuan);
                oRow.ToltalPrice = parseInt(Number(oRow.OrderQuan)) * Number(oRow.NetPrice);
                oComponent_VendCr.getModel("JSON").refresh();
                for (var i = 0; i < allItems.length; i++) {
                    if (allItems[i].Selected) {
                        TotalPrice += parseInt(Number(allItems[i].OrderQuan)) * Number(allItems[i].NetPrice);
                    }
                }
                if (sType !== 'genric') {
                    oComponent_VendCr.getModel("JSON").setProperty("/TotalPrice", TotalPrice.toFixed(2));
                }

            },
            onPressComment: function (oEvent) {
                var oSelectedItem = oEvent.getSource().getBindingContext("JSON").getObject();
                oComponent_VendCr.getModel("JSON").setProperty("/oSelectedRow", oSelectedItem);
                oComponent_VendCr.getModel("JSON").setProperty("/newItemText", oSelectedItem.ExsistItemText);
                this.onOpenDialog('', 'comment');
            },
            saveCommentRow: function (oEvent) {
                var oModel = oComponent_VendCr.getModel("JSON"),
                    oRow = oModel.getProperty("/oSelectedRow");
                oRow.ExsistItemText = oModel.getProperty("/newItemText");
                oComponent_VendCr.getModel("JSON").refresh();
                this.onCloseDialog('', 'comment');
            },
            addOrRemoveFromCart: function (oEvent) {
                var oModel = oComponent_VendCr.getModel("JSON");
                oModel.setProperty("/SelectedItem/Selected", !oModel.getProperty("/SelectedItem/Selected"));
                this.onTableRowSelected();
            },
            handleSortDialogConfirm: function (oEvent) {
                var oTable = this.byId("dataTable"),
                    mParams = oEvent.getParameters(),
                    oBinding = oTable.getBinding("items"),
                    sPath,
                    bDescending,
                    aSorters = [];
                try {
                    sPath = mParams.sortItem.getKey();
                    bDescending = mParams.sortDescending;
                    aSorters.push(new Sorter({ path: sPath, descending: bDescending }));

                } catch (e) { }


                // apply the selected sort and group settings
                oBinding.sort(aSorters);
            },
            selectAllTableItems: function (oEvent) {
                var allItems = oComponent_VendCr._MasterController.getView().byId("dataTable").getItems(),
                    TotalPrice = 0,
                    selected = oEvent.getParameter("selected"),
                    countSelectedRows = selected ? allItems.length : 0;
                for (var i = 0; i < allItems.length; i++) {
                    var item = allItems[i].getBindingContext("JSON").getObject();
                    var path = allItems[i].getBindingContext("JSON").getPath();
                    oComponent_VendCr.getModel("JSON").setProperty(path + '/Selected', selected);
                    if (selected) {
                        TotalPrice += parseInt(Number(item.OrderQuan)) * Number(item.NetPrice);
                    }
                }
                oComponent_VendCr.getModel("JSON").setProperty("/CartCount", countSelectedRows);
                oComponent_VendCr.getModel("JSON").setProperty("/TotalPrice", TotalPrice.toFixed(2));

            },
            onTableRowSelected: function (oEvent, items) {
                try {
                    oEvent.getSource().getBindingContext("JSON").getObject().Selected = oEvent.getParameter("selected");
                } catch (e) { }
                var allItems = items || oComponent_VendCr.getModel("JSON").getProperty("/items"),
                    TotalPrice = 0,
                    countSelectedRows = 0;
                for (var i = 0; i < allItems.length; i++) {
                    if (allItems[i].Selected) {
                        TotalPrice += parseInt(Number(allItems[i].OrderQuan)) * Number(allItems[i].NetPrice);
                        countSelectedRows++;
                    }
                }
                oComponent_VendCr.getModel("JSON").setProperty("/CartCount", countSelectedRows);
                oComponent_VendCr.getModel("JSON").setProperty("/TotalPrice", TotalPrice.toFixed(2));
            },

            showErrorsMsg: function (aErrors) {
                oComponent_VendCr.getModel("JSON").setProperty("/Errors", aErrors);
                this.onOpenMessageViewDialog();

            },
            onOpenMessageViewDialog: function (oEvent) {
                if (!oComponent_VendCr._MessageView) {
                    oComponent_VendCr._MessageView = sap.ui.xmlfragment("zmm_vendor_po_cr.view.popovers." + 'MessageViewDialog', this);
                    this.getView().addDependent(oComponent_VendCr._MessageView);
                }
                oComponent_VendCr._MessageView.open();
            },
            afterMessageViewClose: function (oEvent) {
                oComponent_VendCr._MessageView.destroy();
                oComponent_VendCr._MessageView = undefined;
            },
            onNavBack: function () {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                oCrossAppNavigator.toExternal({
                    target: { shellHash: "#" }
                });

            },
            resetMasterProperties: function (oEvent) {
                oComponent_VendCr.getModel("JSON").setProperty("/items", '');
                oComponent_VendCr.getModel("JSON").setProperty("/ItemsClicked", false);
                oComponent_VendCr.getModel("JSON").setProperty("/DatesError", false);
                oComponent_VendCr.getModel("JSON").setProperty("/OrderNumber", '');
                oComponent_VendCr.getModel("JSON").setProperty("/TotalPrice", 0);
                oComponent_VendCr.getModel("JSON").setProperty("/cartClicked", false);
                oComponent_VendCr.getModel("JSON").setProperty("/CartCount", 0);
                oComponent_VendCr.getModel("JSON").setProperty("/Filters/Matnr", '');
                oComponent_VendCr.getModel("JSON").setProperty("/SelectedAll", false);
                oComponent_VendCr.getModel("JSON").setProperty("/Filters/AtcCode", '');
                oComponent_VendCr.getModel("JSON").setProperty("/Filters/PurchasingGroup", '');
                oComponent_VendCr.getModel("JSON").setProperty("/Filters/BlockedIndicator", '2');
                oComponent_VendCr.getModel("JSON").setProperty("/Filters/Item29", '1');
                oComponent_VendCr.getModel("JSON").setProperty("/Filters/DrugsOnly", false);
                oComponent_VendCr.getModel("JSON").setProperty("/Filters/InventoryDaysSlider", [0, 10000]);
                oComponent_VendCr.getModel("JSON").setProperty("/createDraftClicked", false);
            },
            onCancelOrder: function (oEvent) {

                this.onOpenDialog('', 'CancelOrder');

            },
            onDeleteOrder: function (oEvent) {
                this.onOpenDialog('', 'DeleteOrder');
            },

            validateEmailInput: function (oInput) {
                var sValueState = "None";
                var bValidationError = false;
                var oBinding = oInput.getBinding("value");
                oBinding.setType("Email");

                try {
                    this.validateValue(oInput.getValue());
                } catch (oException) {
                    sValueState = "Error";
                    bValidationError = true;
                }

                oInput.setValueState(sValueState);

                return bValidationError;
            },
            validateValue: function (oValue) {
                // The following Regex is only used for demonstration purposes and does not cover all variations of email addresses.
                // It's always better to validate an address by simply sending an e-mail to it.
                var rexMail = /^\w+[\w-+\.]*\@\w+([-\.]\w+)*\.[a-zA-Z]{2,}$/;
                if (!oValue.match(rexMail)) {
                    throw new ValidateException("'" + oValue + "' is not a valid e-mail address");
                }
            },
            addNewMail: function (oEvent) {
                var mailInput = sap.ui.getCore().byId("emailInput");
                var bValidationError = false;
                bValidationError = this.validateEmailInput(mailInput) || bValidationError;
                if (!bValidationError) {
                    var sMail = mailInput.getValue();
                    var partnersTable = oComponent_VendCr.getModel("JSON").getProperty("/emailsList");
                    partnersTable.push({ EmailAddress: sMail, newRow: true, Selected: true });
                    oComponent_VendCr.getModel("JSON").setProperty("/emailsList", partnersTable);
                    var sMail = mailInput.setValue('');
                }

            },
            deletePartnersRow: function (oEvent) {
                var model = oComponent_VendCr.getModel("JSON");
                var modelData = model.getData();
                var partnersTable = modelData.emailsList;
                let aPath = oEvent.getSource().getBindingContext("JSON").getPath().split("/");
                let indexToDelete = aPath[aPath.length - 1];
                partnersTable.splice(indexToDelete, 1);
                model.refresh(true);

            },
            beforeSaveDraft: function (oEvent) {
                MessageBox.confirm("האם תרצה לשמור את ההזמנה בטיוטה?", {
                    actions: ['ביטול', 'אישור'],
                    emphasizedAction: 'אישור',
                    onClose: function (sAction) {
                        if (sAction === 'אישור') {
                            oComponent_VendCr._MasterController.onCreateOrder('2');
                        }

                    }
                });

            },
            onCreateOrder: function (sType) {
                if (sType === '2' || !window.event.detail || window.event.detail == 1) {
                    var oModel = oComponent_VendCr.getModel("JSON").getData(),
                        aItems = [],
                        aEmails = [],
                        items = sType === '2' || sType === '5' ? oModel.items : oModel.CartItems,
                        emails = oModel.emailsList;
                    if (!!emails && sType === '3') {
                        for (var i = 0; i < emails.length; i++) {
                            if (emails[i].Selected) {
                                aEmails.push({
                                    Checkbox: '',
                                    ExMail: emails[i].newRow ? 'X' : '',
                                    Parnr: emails[i].Parnr || '',
                                    NameText: '',
                                    Vtext: '',
                                    SmtpAddr: emails[i].EmailAddress
                                });
                            }
                        }
                    }
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].Selected) {
                            aItems.push({
                                DeliveryDate: this.getFixedDate(items[i].DelvDate) || this.getFixedDate(oModel.Filters.deliveryDate) || null,
                                Agrmnt: items[i].PurchaseAgreement,
                                MatType: items[i].MatType,
                                AgrmntItem: items[i].PurchaseAgreementLine,
                                Item: items[i].Item,
                                Matnr: items[i].Matnr,
                                Quantity: (items[i].OrderQuan).toString() || '0',
                                Unit: items[i].RecommendedUnit,
                                ItemText: items[i].ExsistItemText || '',
                                Vendor: items[i].Vendor

                            });
                        }
                    }
                    var oEntry = {
                        IvActivity: sType,
                        IvAltMat: false,
                        IvReturnSlocInd: oModel.newOrder.LgortFlag,
                        IvOrderNum: oModel.OrderNumber || '',
                        IvOrderText: oModel.newOrder.OrderText || '',
                        IvPriceOutput: oModel.newOrder.PriceOutput || false,
                        IvPurGroup: '',
                        IvVendor: oModel.Filters.Vendor,
                        OrderEmails: aEmails,
                        OrderItems: aItems,
                        OrderNumber: [],
                        Partner: [],
                        Return: []

                    }

                    models.createOrder(oEntry).then(function (data) {
                        if (data.Return.results.length > 0) {
                            this.showErrorsMsg(data.Return.results);
                        }
                        else if (sType === '1') {
                            var navCon = sap.ui.getCore().byId("navCon");
                            navCon.to("page2", "Slide");
                        }
                        else if (sType === '3') {
                            oComponent_VendCr.getModel("JSON").setProperty("/orderNumbers", data.OrderNumber.results);
                            this.SaveAndSand();

                        }
                        else if (sType === '2') {
                            oComponent_VendCr.getModel("JSON").setProperty("/orderNumbers", data.OrderNumber.results);
                            this.onOpenDialog("", 'SaveDraft');
                        }
                        else if (sType === '5') {
                            this.onCloseDialog('', 'DeleteOrder');
                            this.onOpenDialog('', 'CancelOrderSuccess');
                        }

                    }.bind(this)).catch(function (error) {
                        console.log(error);
                        try {
                            MessageBox.error(JSON.parse(error.responseText).error.message.value);
                        } catch (e) {
                            MessageBox.error(JSON.stringify(error));
                        }
                    });
                }
            },
            onlyInputNumbers: function (oEvent) {
                var value = oEvent.getSource().getValue().replace(/[^\d]/g, '');
                oEvent.getSource().setValue(value);
            },
            GoToTransaction: function (event, sType, value1) {
                var semanticObject, action, oParams;
                var oModelData = oComponent_VendCr.getModel("JSON").getData().Filters;
                switch (sType) {
                    case 'ME33K':
                        semanticObject = 'ME33K';
                        action = 'display';
                        oParams = {
                            IvAgreement: value1
                        };
                        break;
                    case 'MMPURPAMEPO':
                        semanticObject = 'MMPURPAMEPO';
                        action = 'display';
                        oParams = {
                            PurchaseOrder: value1
                        };
                        break;
                    case 'MM03':
                        semanticObject = 'MM03';
                        action = 'display';
                        oParams = {
                            IvMaterial: value1
                        };
                        break;
                    case 'ME23N':
                        semanticObject = 'ME23N';
                        action = 'display';
                        oParams = {
                            IvOrder: value1
                        };
                        break;
                    case 'MB51':
                        semanticObject = 'MB51';
                        action = 'display';
                        oParams = {
                            IvMaterial: value1,
                            IvPlant: '1000',
                            IvStorageLocation: value3
                        };
                        break;
                    case 'MB52':
                        semanticObject = 'MB52';
                        action = 'display';
                        oParams = {
                            IvMaterial: value1,
                            IvPlant: '1000',
                            IvStorageLocation: value3
                        };
                        break;
                    case 'Material':
                        semanticObject = 'Material';
                        action = 'displayStockMultipleMaterials';
                        oParams = {
                            Material: value1,
                            Plant: '1000',
                            StorageLocation: ['SH10', 'SH30', 'SH60', 'NL10']

                        };
                        break;
                    case 'MaterialMovement':
                        semanticObject = 'MaterialMovementNew';
                        action = 'displayList';
                        oParams = {
                            Material: value1,
                            Plant: '1000',
                            StorageLocation: ['SH60', 'NL10'],
                            GoodsMovementType: ['351', '352']
                        };
                        break;

                }
                this.navToTransaction(event, semanticObject, action, oParams);
            },
            goToApp: function (oEvent, orderNum) {
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = "zmm_create_vendor_po-create";
                var actionURL = "create&/master/" + orderNum;
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: 'zmm_create_vendor_po',
                                    action: actionURL
                                }
                            })) || "";
                            sap.m.URLHelper.redirect(hash, true);
                        }

                    })
                    .fail(function () { });
            },
            navToTransaction: function (event, sSemanticObject, sAction, oParams) {

                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                var semamticActionObj = sSemanticObject + '-' + sAction;
                oCrossAppNavigator.isIntentSupported([semamticActionObj])
                    .done((aResponses) => {
                        if (aResponses[semamticActionObj].supported === true) {
                            var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
                                target: {
                                    semanticObject: sSemanticObject,
                                    action: sAction
                                },
                                params: oParams
                            })) || "";
                            sap.m.URLHelper.redirect(hash, true);
                        }

                    })
                    .fail(function () { });
            },
            onExportWithEbeln: function (oEvent, sEbeln) {
                models.LoadExcel(sEbeln).then((data) => {
                    oComponent_VendCr.getModel("JSON").setProperty("/ExcelData", data);
                    this.buildExcel(data.results, sEbeln);

                }).catch((error) => {
                    models.handleErrors(error);
                });

            },

            buildExcel: function (aItems, sEbeln) {
                var oSettings, oSheet,
                    aCols = [];

                aCols.push({
                    label: oComponent_VendCr.i18n('OrderNum'),
                    property: 'Ebeln'
                });

                aCols.push({
                    label: oComponent_VendCr.i18n('VendorCode'),
                    property: 'Lifnr'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('VendorName'),
                    property: 'Name1Vendor'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('invoiceProducer'),
                    property: 'Lifre'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('invoiceProducerName'),
                    property: 'Name1Invoice'
                });

                aCols.push({
                    label: oComponent_VendCr.i18n('orderDate'),
                    property: 'Bedat',
                    type: EdmType.Date
                });

                aCols.push({
                    label: oComponent_VendCr.i18n('orderItem'),
                    property: 'Ebelp'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('Material'),
                    property: 'Matnr'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('MaterialDesc'),
                    property: 'Txz01'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('quantity'),
                    property: 'Menge'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('Unit'),
                    property: 'Meins'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('netPrice'),
                    property: 'Netpr'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('orderCurrency'),
                    property: 'Waers'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('storageLocation'),
                    property: 'Lgort'
                });
                aCols.push({
                    label: oComponent_VendCr.i18n('storageLocationDesc'),
                    property: 'Lgobe'
                });
                oSettings = {
                    workbook: {
                        columns: aCols
                    },
                    dataSource: aItems,
                    fileName: ":הזמנה" + sEbeln
                };

                oSheet = new Spreadsheet(oSettings);
                oSheet.build()
                    .then(function () { });

            },
            getFixedDate: function (value) {
                if (!value) {
                    return;
                }
                var before = value.getTime();
                var after = new Date(before + 10800000); //adding three hours to fix UTC gateway processing day back
                return after;
            },
            getFixedDate: function (value) {
                if (!value) {
                    return;
                }
                var before = value.getTime();
                var after = new Date(before + 10800000); //adding three hours to fix UTC gateway processing day back
                return after;
            }
        });
    });

