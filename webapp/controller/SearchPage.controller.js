/* global oComponent_VendCr : true*/
sap.ui.define([
	"zmm_vendor_po_cr/controller/BaseController",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/Fragment",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"zmm_vendor_po_cr/model/models",
	'sap/m/MessageToast'
], function (BaseController, Controller, JSONModel, Fragment, Filter, FilterOperator, models, MessageToast) {
	"use strict";

	return BaseController.extend("zmm_vendor_po_cr.controller.SearchPage", {
		onInit: function () {
			oComponent_VendCr._searchController = this;
			this.oRouter = this.getOwnerComponent().getRouter();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute('').attachPatternMatched(this.SearchPageRoute, this);
			oRouter.getRoute('SearchPage').attachPatternMatched(this.SearchPageRoute, this);
		},
		SearchPageRoute: function (event) {
			this.resetMasterProperties();

		},
		NavToReview: function (sPage) {
			this.oRouter.navTo("master");
			// var navCon = oComponent_VendCr._searchController.byId("navCon");
			// navCon.to(oComponent_VendCr._searchController.byId("Review"), "Slide");
		},
		onValueHelpSearch: function (oEvent, sType) {
			var sValue = oEvent.getParameter("value");
			var oFilter = [];
			if (sType === 'Vendor') {
				oFilter = new Filter({
					filters: [new Filter("Lifnr", FilterOperator.Contains, sValue),
					new Filter("LifnrTxt", FilterOperator.Contains, sValue.toUpperCase())
					],
					and: false
				});
			}
			var oBinding = oEvent.getParameter("itemsBinding");
			oBinding.filter(oFilter);
		},
		onSelectOrderType: function (oEvent) {
			debugger;
			var sVendorName = oComponent_VendCr.getModel("JSON").getProperty("/Filters/VendorName");		
			if(!!sVendorName){
				setTimeout(function () { oComponent_VendCr.getModel("JSON").setProperty("/Filters/VendorName" , sVendorName); return; }, 200);
				
			}
			
		},

		onValueHelpRequest: function (oEvent) {
			var sInputValue = oEvent.getSource().getValue(),
				oView = this.getView();

			if (!this._SupplierDialog) {
				oComponent_VendCr._searchController._SupplierDialog = sap.ui.xmlfragment("ZMM_VENDOR_PO_CR.view.fragments.SupplierDialog", oComponent_VendCr._searchController);
				oComponent_VendCr._searchController.getView().addDependent(this._SupplierDialog);

			}
			this._SupplierDialog.open();
			// this._SupplierDialog.then(function(oDialog) {
			// 	// Create a filter for the binding
			// 	oDialog.getBinding("items").filter([new Filter("Name", FilterOperator.Contains, sInputValue)]);
			// 	// Open ValueHelpDialog filtered by the input's value
			// 	oDialog.open(sInputValue);
			// });
		},
		navToCreateOrder: function () {
			var aFilters = oComponent_VendCr.getModel("JSON").getProperty("/Filters"),
				beginDate = oComponent_VendCr._searchController.getView().byId("SearchDateFrom"),
				endDate = oComponent_VendCr._searchController.getView().byId("SearchDateEnd"),
				deliveryDate = oComponent_VendCr._searchController.getView().byId("deliveryDate"),
				matnr = oComponent_VendCr._searchController.getView().byId("MatnrInput");
			oComponent_VendCr.getModel("JSON").setProperty("/ItemsClicked", true)
			if (aFilters.orderType === '02' && aFilters.Matnrs.length > 0) {
				var sFirstVendor = aFilters.Matnrs[0].vendor;
				for (var i = 1; i < aFilters.Matnrs.length; i++) {
					if (sFirstVendor !== aFilters.Matnrs[i].vendor) {
						matnr.setValueState("Error");
						MessageToast.show(oComponent_VendCr.i18n('MantrMsgError'), {
							duration: 3500
						});
						return;
					}
				}
			}
			if (beginDate.getValueState() === 'Error' || endDate.getValueState() === 'Error' || deliveryDate.getValueState() === 'Error') {
				MessageToast.show(oComponent_VendCr.i18n('SearchPageErrorMsg2'), {
					duration: 1500
				});
				return;
			}
			else if (aFilters.BeginDate >= aFilters.EndDate) {
				endDate.setValueState("Error");
				// oComponent_VendCr.getModel("JSON").setProperty("/DatesError" ,true);
				MessageToast.show(oComponent_VendCr.i18n('SearchPageDatesErrorMsg'), {
					duration: 1500
				});
			}
			else if ((aFilters.orderType === '01' && !aFilters.Vendor) || (aFilters.orderType === '02' && aFilters.Matnrs.length === 0)) {
				MessageToast.show(oComponent_VendCr.i18n('SearchPageErrorMsg'), {
					duration: 1500
				});
			}
			else if (!!aFilters.BeginDate && !!aFilters.EndDate && Number(aFilters.DeliveryDays) > 0 && Number(aFilters.DeliveryDays) < 256) {
				beginDate.setValueState("None");
				endDate.setValueState("None");
				var sMatType = oComponent_VendCr.getModel("JSON").getProperty("/Filters/MatType");
				oComponent_VendCr.getModel("JSON").setProperty("/Filters/MainMatType", sMatType);
				this.oRouter.navTo("master");
			}
			else if (Number(aFilters.DeliveryDays) > 255) {
				MessageToast.show(oComponent_VendCr.i18n('DaysForDeliveryErrorMsg'), {
					duration: 1500
				});
			}
			else {
				MessageToast.show(oComponent_VendCr.i18n('SearchPageErrorMsg'), {
					duration: 1500
				});
			}

		}

	});
});