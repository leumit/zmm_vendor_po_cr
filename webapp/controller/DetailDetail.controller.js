/* global oComponent_VendCr: true */
sap.ui.define([
	"zmm_vendor_po_cr/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function (Controller ,JSONModel) {
	"use strict";

	return Controller.extend("zmm_vendor_po_cr.controller.DetailDetail", {
		onInit: function () {
			var oExitButton = this.getView().byId("exitFullScreenBtn"),
				oEnterButton = this.getView().byId("enterFullScreenBtn");

			this.oRouter = this.getOwnerComponent().getRouter();
			this.oModel = this.getOwnerComponent().getModel();

			this.oRouter.getRoute("detailDetail").attachPatternMatched(this._onSupplierMatched, this);
			this.oRouter.getRoute("detailDetailFull").attachPatternMatched(this._onFullMatched, this);
			
			[oExitButton, oEnterButton].forEach(function (oButton) {
				oButton.addEventDelegate({
					onAfterRendering: function () {
						if (this.bFocusFullScreenButton) {
							this.bFocusFullScreenButton = false;
							oButton.focus();
						}
					}.bind(this)
				});
			}, this);
		},
		handleAboutPress: function () {
			var oNextUIState = this.getOwnerComponent().getHelper().getNextUIState(3);
			this.oRouter.navTo("page2", {layout: oNextUIState.layout});
		},
		handleFullScreen: function () {
			this.bFocusFullScreenButton = true;
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/endColumn/fullScreen");
			this.oRouter.navTo("detailDetailFull", {product: this._product, supplier: this._supplier});
		},
		handleExitFullScreen: function () {
			this.bFocusFullScreenButton = true;
		//	var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/endColumn/exitFullScreen");
			this.oRouter.navTo("detailDetail", {product: this._product, supplier: this._supplier});
		},
		handleClose: function () {
			//var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/endColumn/closeColumn");
			this.oRouter.navTo("detail", {product: this._product});
		},
		_onFullMatched: function(oEvent){
			this._product = oEvent.getParameter("arguments").product || this._product || "0";
			oComponent_VendCr._fcl.byId("fcl").setLayout("EndColumnFullScreen");
			oComponent_VendCr._fcl._updateUIElements();
			this._bindElement();
		},
		_onSupplierMatched: function (oEvent) {
			this._supplier = oEvent.getParameter("arguments").supplier || this._supplier || "0";
			this._product = oEvent.getParameter("arguments").product || this._product || "0";
			oComponent_VendCr._fcl.byId("fcl").setLayout("ThreeColumnsMidExpanded");
			oComponent_VendCr._fcl._updateUIElements();
			this._bindElement();
		},
		_bindElement: function(){
			this.getView().bindElement({
				path: "/ProductCollectionStats/Filters/1/values/" + this._supplier,
				model: "products"
			});	
		}
	});
});