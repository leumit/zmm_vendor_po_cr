/* global oComponent_VendCr: true */
sap.ui.define([
	"zmm_vendor_po_cr/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"zmm_vendor_po_cr/model/formatter",
	"zmm_vendor_po_cr/model/models",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/viz/ui5/format/ChartFormatter',
	'sap/m/MessageToast',
	"sap/m/MessageBox"
], function (Controller, JSONModel, formatter, models, Filter, FilterOperator, ChartFormatter, MessageToast, MessageBox) {
	"use strict";

	return Controller.extend("zmm_vendor_po_cr.controller.Detail", {
		formatter: formatter,
		onInit: function () {
			oComponent_VendCr._detailController = this;
			var oExitButton = this.getView().byId("exitFullScreenBtn"),
				oEnterButton = this.getView().byId("enterFullScreenBtn");
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oModel = this.getOwnerComponent().getModel();

			this.oRouter.getRoute("detail").attachPatternMatched(this._onOrderMatched, this);
			this.oRouter.getRoute("detailFull").attachPatternMatched(this._onFullMatched, this);
		},

		handleItemPress: function (oEvent) {
			var oNextUIState = this.getOwnerComponent().getHelper().getNextUIState(2),
				supplierPath = oEvent.getSource().getBindingContext("products").getPath(),
				supplier = supplierPath.split("/").slice(-1).pop();

			this.oRouter.navTo("detailDetail", {
				OrderNo: this._product,
				supplier: supplier
			});
		},
		handleFullScreen: function (sMaterial) {
			this.bFocusFullScreenButton = true;
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/fullScreen");
			var sOrderNum = oComponent_VendCr.getModel("JSON").getProperty("/OrderNumber") || '';
			this.oRouter.navTo("detailFull", {
				Material: sMaterial ,
				OrderNumber: sOrderNum

			});
		},
		handleExitFullScreen: function (sMaterial) {
			this.bFocusFullScreenButton = true;
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/exitFullScreen");
			var sOrderNum = oComponent_VendCr.getModel("JSON").getProperty("/OrderNumber") || '';
			this.oRouter.navTo("detail", {
				Material: sMaterial,
				OrderNumber: sOrderNum
			});
		},
		handleClose: function () {
			var sNextLayout = this.oModel.getProperty("/actionButtonsInfo/midColumn/closeColumn");
			var sOrderNum = oComponent_VendCr.getModel("JSON").getProperty("/OrderNumber") || '';
			this.oRouter.navTo("master", {
				OrderNumber: sOrderNum
			});
		},
		_onFullMatched: function (oEvent) {
			oComponent_VendCr.getModel("JSON").setProperty("/layout", 'TwoColumnsMidExpanded');
			oComponent_VendCr._fcl.byId("fcl").setLayout("MidColumnFullScreen");
			var items = oComponent_VendCr.getModel("JSON").getProperty("/items");
			if (!items) {
				oComponent_VendCr._detailController.navTo('', 'SearchPage');
			}
		},
		onPressCreateDraft: function (oEvent) {
			oComponent_VendCr.getModel("JSON").setProperty("/createDraftClicked", true);
			var aItems = [];
			var rows = oComponent_VendCr.getModel("JSON").getProperty("/OrderGenerics");
			for (var i = 0; i < rows.length; i++) {
				if (!!rows[i].Selected) {
					if (Number(rows[i].OrderQuan) === 0) {
						MessageToast.show(oComponent_VendCr.i18n('noCountMsg'), {
							duration: 1500
						});
						return;
					}
					else {
						aItems.push({
							Agrmnt: rows[i].PurchaseAgreement,
							MatType: rows[i].MatType,
							AgrmntItem: rows[i].PurchaseAgreementLine,
							Item: '',
							Matnr: rows[i].Matnr,
							Quantity: (rows[i].OrderQuan).toString(),
							Unit: rows[i].RecommendedUnit,
							ItemText: rows[i].ExsistItemText || '',
							Vendor: rows[i].Vendor
						});
					}

				}
			}
			if (aItems.length < 1) {
				MessageToast.show(oComponent_VendCr.i18n('noSelectedItemsMsg'), {
					duration: 1500
				});
			}
			else {
				oComponent_VendCr.getModel("JSON").setProperty("/OrderGenericsItems", jQuery.extend(true, [], aItems));
				this.onCreateGenricDraft(aItems);
			}
		},
		onCreateGenricDraft: function (aItems) {
			var oModel = oComponent_VendCr.getModel("JSON").getData(),
				aErrors = [];
			var oEntry = {
				IvActivity: '2',
				IvAltMat: true,
				IvReturnSlocInd: false,
				IvOrderNum: '',
				IvOrderText: '',
				IvPriceOutput: false,
				IvPurGroup: '',
				IvVendor: '',
				OrderEmails: [],
				OrderItems: aItems,
				OrderNumber: [],
				Partner: [],
				Return: []

			}
			models.createOrder(oEntry).then(function (data) {
				if (data.Return.results.length > 0) {
					this.showErrorsMsg(data.Return.results);
				}
				else{
					var sMatnr = oModel.SelectedItem.Matnr;
					models.LoadGenericsMaterials(sMatnr).then((data) => {
						MessageToast.show(oComponent_VendCr.i18n('GenericDraftSuccessMsg'), {
							duration: 2000
						});
						for(var i = 0; i<data.results.length; i++){
							data.results[i].ToltalPrice = (parseInt(Number(data.results[i].OrderQuan)) * Number(data.results[i].NetPrice)).toFixed(2);
						}	
						oComponent_VendCr.getModel("JSON").setProperty("/createDraftClicked", false);
						oComponent_VendCr.getModel("JSON").setProperty("/OrderGenerics", data.results);
						oComponent_VendCr.getModel("JSON").setProperty("/genricMatnr", sMatnr);
					}).catch((error) => {
						models.handleErrors(error);
					});
				}
			}).catch(function (error) {
				console.log(error);
				try {
					MessageBox.error(JSON.parse(error.responseText).error.message.value);
				} catch (e) {
					MessageBox.error(JSON.stringify(error));
				}
			});

			


		},
		onFilterIssues: function (oEvent ,VizFrame) {
			var sQuery = oComponent_VendCr.getModel("JSON").getProperty("/IssuesRegion");
			var oFilter = [new Filter('RegionCode', FilterOperator.Contains, sQuery)];
			if(!VizFrame){
				oFilter.push(new Filter('TopInd', FilterOperator.EQ, true));
			}
			var growthRatesDataSet = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'SlocDesc',
					value: "{JSON>SlocDesc}"
				}],
				measures: [{
					name: "IssueQuan",
					value: "{JSON>IssueQuan}"
				},
				{
					name: "LackQuan",
					value: "{JSON>LackQuan}"
				}],
				data: {
					path: "JSON>/OrderIssues/ItemIssueLn/results",
					filters: [oFilter]
				}
			});
			//assign "new" dataset 
			var vizFrame = VizFrame || this.getView().byId("idVizFrame");
			vizFrame.setDataset(growthRatesDataSet);
		},
		_onDetailDetailMatched: function (oEvent) {
			oComponent_VendCr.getModel("JSON").setProperty("/layout", 'TwoColumnsMidExpanded');
			
		},
		_onOrderMatched: function (oEvent) {
			var sOrderNo = oEvent.getParameter("arguments")["OrderNo"];
			oComponent_VendCr.getModel("JSON").setProperty("/layout", 'TwoColumnsMidExpanded');
			oComponent_VendCr._fcl.byId("fcl").setLayout("TwoColumnsMidExpanded");
			oComponent_VendCr._fcl._updateUIElements();
			var items = oComponent_VendCr.getModel("JSON").getProperty("/items");
			if (!items) {
				oComponent_VendCr._MasterController.navTo('', 'SearchPage');
			}
			
		},
		_bindElement: function () {
		
		},
		onOpenGrList: function (oEvent) {
			var event = jQuery.extend(true, {}, oEvent);
			var items = jQuery.extend(true, [], oEvent.getSource().getBindingContext("JSON").getObject().GrList);
			items.splice(0, 1);
			items.splice(1, 1);
			oComponent_VendCr.getModel("JSON").setProperty("/GrItems", items);
			this.onOpenPopover(event, 'History');

		},
		onDetailsTabSelected: function (oEvent) {
			var sKey = oEvent.getParameter("key");
			var oModelData = oComponent_VendCr.getModel("JSON").getData(),
				sVendor = oModelData.SelectedItem.Vendor,
				sMatnr = oModelData.SelectedItem.Matnr;
			switch (sKey) {
				case 'GenericsMaterials':
					if (oModelData.genricMatnr !== sMatnr) {
						models.LoadGenericsMaterials(sMatnr).then((data) => {
							oComponent_VendCr.getModel("JSON").setProperty("/createDraftClicked", false);
							for(var i = 0; i<data.results.length; i++){
								data.results[i].ToltalPrice = (parseInt(Number(data.results[i].OrderQuan)) * Number(data.results[i].NetPrice)).toFixed(2);
							}					
							oComponent_VendCr.getModel("JSON").setProperty("/OrderGenerics", data.results);
							oComponent_VendCr.getModel("JSON").setProperty("/genricMatnr", sMatnr);
						}).catch((error) => {
							models.handleErrors(error);
						});
					}
					break;
				case 'Issues':
					models.LoadIssues(sMatnr).then((data) => {
						var totalLack = 0;
						var aTopIssues = [];
						for (var i = 0; i < data.ItemIssueLn.results.length; i++) {
							data.ItemIssueLn.results[i].IssueQuan = parseInt(data.ItemIssueLn.results[i].IssueQuan);
							data.ItemIssueLn.results[i].LackQuan = parseInt(data.ItemIssueLn.results[i].LackQuan);
							totalLack += parseInt(data.ItemIssueLn.results[i].LackQuan);
							
						}
						oComponent_VendCr.getModel("JSON").setProperty("/OrderIssues", data);						
						oComponent_VendCr.getModel("JSON").setProperty("/OrderIssues/totalLack", totalLack);
						this.buildGraph();
					}).catch((error) => {

						models.handleErrors(error);
					});

					break;
				case 'orderHistory':

					models.LoadOrderHistory(sMatnr, sVendor).then((data) => {
						for (var i = 0; i < data.ItmPoHstGRSet.results.length; i++) {
							for (var j = 0; j < data.ItmPoHstLnSet.results.length; j++) {
								if (data.ItmPoHstGRSet.results[i].OrderNum === data.ItmPoHstLnSet.results[j].OrderNum) {
									data.ItmPoHstLnSet.results[j].GrList = [];
									data.ItmPoHstLnSet.results[j].GrList.push(data.ItmPoHstGRSet.results[i]);
									break;
								}
							}

						}
						try {
							data.ItmPoHstLnSet.results[data.ItmPoHstLnSet.results.length - 1].last = true;
						} catch (e) {
						}

						oComponent_VendCr.getModel("JSON").setProperty("/OrderHistory", data);
					}).catch((error) => {

						models.handleErrors(error);
					});

					break;
			}
		},
		buildGraph: function () {
			var _graph = this.byId("idVizFrame");
			var oProperties = {
				plotArea: {
					dataLabel: {
						visible: true,
						showTotal: true,
						hideWhenOverlap: false,
					},
					dataPointStyle: {
						"rules":
							[
								{
									"dataContext": { "IssueQuan": { "min": 0 } },
									"properties": {
										"color": "sapUiChartPaletteQualitativeHue1"

									},
									"displayName": "ניפוק"
								},
								{
									"dataContext": { "LackQuan": { "min": 0 } },
									"properties": {
										"color": "sapUiChartPaletteSemanticBad"
									},
									"displayName": "חוסר"
								},


							]
					}, primaryScale: {
						fixedRange: true
					}
				},
				valueAxis: {
					title: {
						text: window.external.DataContext === undefined ? oComponent_VendCr.i18n("unitsCount") : 'תודיחי תומכ'
					}
				},
				categoryAxis: {
					title: {
						text: window.external.DataContext === undefined ?oComponent_VendCr.i18n("Pharmacy") : 'תחקרמ תיב'
					}

				},
				title: {
					visible: true,
					text: '',
					alignment: "right"
				}
			};
			_graph.setVizProperties(oProperties);
			this.onFilterIssues();
		},
		createFrameDialog: function (graphDialog) {
			var oProperties = {
				plotArea: {
					dataLabel: {
						visible: true,
						showTotal: true,
						hideWhenOverlap: false,
					},
					dataPointStyle: {
						"rules":
							[
								{
									"dataContext": { "IssueQuan": { "min": 0 } },
									"properties": {
										"color": "sapUiChartPaletteQualitativeHue1"

									},
									"displayName": oComponent_VendCr.i18n("Issue")
								},
								{
									"dataContext": { "LackQuan": { "min": 0 } },
									"properties": {
										"color": "sapUiChartPaletteSemanticBad"
									},
									"displayName": oComponent_VendCr.i18n("Lack")
								},


							]
					}, primaryScale: {
						fixedRange: true
					}
				},
				valueAxis: {
					title: {
						text: window.external.DataContext === undefined ? oComponent_VendCr.i18n("unitsCount") : 'תודיחי תומכ'
					}
				},
				categoryAxis: {
					title: {
						text: window.external.DataContext === undefined ?oComponent_VendCr.i18n("Pharmacy") : 'תחקרמ תיב'
					}

				},
				title: {
					visible: true,
					text: '',
					alignment: "right"
				}
			};
			graphDialog.setVizProperties(oProperties);
			this.onFilterIssues('',graphDialog);

		},
	});
});