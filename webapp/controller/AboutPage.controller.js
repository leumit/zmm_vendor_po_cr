sap.ui.define([
	"zmm_vendor_po_cr/controller/BaseController",
	"sap/ui/model/json/JSONModel"
], function (Controller,JSONModel) {
	"use strict";

	return Controller.extend("zmm_vendor_po_cr.controller.AboutPage", {
		onInit: function () {
			
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oModel = this.getOwnerComponent().getModel();
		},
		onBack: function () {
			window.history.go(-1);
			
		}
	});
});