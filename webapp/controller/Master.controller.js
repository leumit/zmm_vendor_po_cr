/* global oComponent_VendCr: true */
sap.ui.define([
	"zmm_vendor_po_cr/controller/BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	'sap/ui/model/Sorter',
	'sap/m/MessageBox',
	"zmm_vendor_po_cr/model/models",
	"zmm_vendor_po_cr/model/formatter",
	"sap/ui/export/Spreadsheet",
	'sap/m/MessageToast',

], function (Controller, JSONModel, Filter, FilterOperator, Sorter, MessageBox, models, formatter, Spreadsheet, MessageToast) {
	"use strict";

	return Controller.extend("zmm_vendor_po_cr.controller.Master", {
		formatter: formatter,
		onInit: function () {
			oComponent_VendCr._MasterController = this;
			this.oRouter = this.getOwnerComponent().getRouter();
			this.oRouter.getRoute("master").attachPatternMatched(this._onMasterMatched, this);
			this._bDescendingSort = false;
			var bus = sap.ui.getCore().getEventBus();
			bus.subscribe("Scanner", "ScannerEvent", this.getScanFile, this);
		},
		_onMasterMatched: function (oEvent) {
			oComponent_VendCr._fcl.byId("fcl").setLayout("OneColumn");
			oComponent_VendCr._fcl._updateUIElements();
			oComponent_VendCr.getModel("JSON").setProperty("/Filters/freeSearch", '');
			var items = oComponent_VendCr.getModel("JSON").getProperty("/items");
			var sOrderNum = oEvent.getParameter("arguments").OrderNumber;
			oComponent_VendCr.getModel("JSON").setProperty("/OrderNumber", sOrderNum);
			if (!items) {
				this.getItems(sOrderNum);

			}
			else {
				oComponent_VendCr._MasterController.filterItems();
			}


		},
		getItems: function (sOrderNum, oDeliveryDate) {
			var aMatnrs = [];
			var oModelData = oComponent_VendCr.getModel("JSON").getData().Filters;
			if (oModelData.orderType === '02') {
				var items = oModelData.Matnrs;
				for (var i = 0; i < items.length; i++) {
					aMatnrs.push({
						Sign: "I",
						Option: "EQ",
						MatnrLow: items[i].Matnrs,
						MatnrHigh: ""
					});
				}
			}
			var oEntry = {
				IvDeliveryDays: Number(oModelData.DeliveryDays),
				IvIssueFrom: this.getFixedDate(oModelData.BeginDate),
				IvIssueTo: this.getFixedDate(oModelData.EndDate),
				IvMatType: oModelData.MatType || '',
				IvOpenPoDays: Number(oModelData.OpenPoDays) || 30,
				IvOpenPoInd: oModelData.OpenPoFlag,
				IvOrderNum: sOrderNum || '',
				IvVendor: oModelData.orderType === '01' ? oModelData.Vendor : oModelData.Matnrs[0].vendor,
				MaterialDataItems: [],
				MaterialDataMatnrs: oModelData.orderType === '02' ? aMatnrs : []


			}

			models.LoadItems(oEntry).then((data) => {
				if (data.MaterialDataItems.results.length === 0) {
					MessageToast.show(oComponent_VendCr.i18n('noRowsMsg'), {
						duration: 1500,
						my: "center center",
						at: "center center"
					});
					setTimeout(function () { oComponent_VendCr._MasterController.navTo('', 'SearchPage'); return; }, 1000);

				}
				else {
					var aMatnr = [],
						aAtc = [],
						aPurchasingGroupArray = [],
						iSelected = 0;
					for (var i = 0; i < data.MaterialDataItems.results.length; i++) {
						if (data.MaterialDataItems.results[i].Selected) {
							iSelected++;
						}
						aMatnr.push({
							"Matnr": data.MaterialDataItems.results[i].Matnr,
							"MatDescription": data.MaterialDataItems.results[i].MatDescription
						});
						aAtc.push({
							"AtcCode": data.MaterialDataItems.results[i].AtcCode,
							"AtcDesc": data.MaterialDataItems.results[i].MatDescription
						});
						aPurchasingGroupArray.push({
							"PurchasingGroup": data.MaterialDataItems.results[i].PurchasingGroup,
							"PurchasingGroupDescription": data.MaterialDataItems.results[i].PurchasingGroupDescription
						});

						data.MaterialDataItems.results[i].RecommendedQuanInt = parseInt(data.MaterialDataItems.results[i].RecommendedQuan);
						data.MaterialDataItems.results[i].ToltalPrice = (parseInt(Number(data.MaterialDataItems.results[i].OrderQuan)) * Number(data.MaterialDataItems.results[i].NetPrice)).toFixed(2);
						data.MaterialDataItems.results[i].OrderQuan = parseInt(data.MaterialDataItems.results[i].OrderQuan) === 0 ? '' : parseInt(data.MaterialDataItems.results[i].OrderQuan);
						data.MaterialDataItems.results[i].MatnrInt = parseInt(data.MaterialDataItems.results[i].Matnr);
						data.MaterialDataItems.results[i].NetPriceFloat = parseFloat(data.MaterialDataItems.results[i].NetPrice);
						data.MaterialDataItems.results[i].AllCurrentStockFloat = parseFloat(data.MaterialDataItems.results[i].AllCurrentStock);
						data.MaterialDataItems.results[i].IssueAvgFloat = parseFloat(data.MaterialDataItems.results[i].IssueAvg);
						data.MaterialDataItems.results[i].BoxUnitOrRoundValueInt = parseInt(data.MaterialDataItems.results[i].BoxUnitOrRoundValue);
						if (!!sOrderNum && !!oDeliveryDate) {
							data.MaterialDataItems.results[i].DelvDate = oDeliveryDate;
						}
					}
					aMatnr = oComponent_VendCr._MasterController.removeDuplicates(aMatnr, "Matnr");
					aAtc = oComponent_VendCr._MasterController.removeDuplicates(aAtc, "AtcCode");
					aPurchasingGroupArray = oComponent_VendCr._MasterController.removeDuplicates(aPurchasingGroupArray, "PurchasingGroup");
					oComponent_VendCr.getModel("JSON").setProperty("/MatnrArray", aMatnr);
					oComponent_VendCr.getModel("JSON").setProperty("/PurchasingGroupArray", aPurchasingGroupArray);
					oComponent_VendCr.getModel("JSON").setProperty("/AtcArray", aAtc);
					oComponent_VendCr.getModel("JSON").setProperty("/items", data.MaterialDataItems.results);
					oComponent_VendCr.getModel("JSON").setProperty("/CartCount", iSelected);
					if (!!sOrderNum) {
						oComponent_VendCr.getModel("JSON").setProperty("/DraftDeliveryDate", data.MaterialDataItems.results[0].DelvDate || new Date());
						oComponent_VendCr.getModel("JSON").setProperty("/DraftFilters/deliveryDate", data.MaterialDataItems.results[0].DelvDate || new Date());
						oComponent_VendCr.getModel("JSON").setProperty("/Filters/Vendor", data.MaterialDataItems.results[0].Vendor);
						oComponent_VendCr.getModel("JSON").setProperty("/Filters/VendorName", data.MaterialDataItems.results[0].VendorName);
						this.onTableRowSelected();
					}
					if (oModelData.orderType === '02') {
						oComponent_VendCr.getModel("JSON").setProperty("/Filters/Vendor", oModelData.Matnrs[0].vendor);
						oComponent_VendCr.getModel("JSON").setProperty("/Filters/VendorName", oModelData.Matnrs[0].vendorName);
					}
					oComponent_VendCr._MasterController.filterItems();
				}

			}).catch((error) => {
				models.handleErrors(error);
			});
		},
		getDraftData: function (oEvent) {
			var oFilters = oComponent_VendCr.getModel("JSON").getProperty("/DraftFilters"),
				sOrderNumber = oComponent_VendCr.getModel("JSON").getProperty("/OrderNumber");
			oComponent_VendCr.getModel("JSON").setProperty("/ItemsClicked", true);
			if (Number(oFilters.DeliveryDays) > 255) {
				MessageToast.show(oComponent_VendCr.i18n('DaysForDeliveryErrorMsg'), {
					duration: 1500
				});
			}
			else if (!oFilters.deliveryDate || Number(oFilters.DeliveryDays) <= 0) {
				MessageToast.show(oComponent_VendCr.i18n('SearchPageErrorMsg2'), {
					duration: 1500
				});

			}
			else {
				oComponent_VendCr.getModel("JSON").setProperty("/Filters/DeliveryDays", oFilters.DeliveryDays);
				oComponent_VendCr.getModel("JSON").setProperty("/Filters/deliveryDate", oFilters.deliveryDate);
				this.getItems(sOrderNumber, oFilters.deliveryDate);
				this.onCloseDialog('', 'EditDraft');
			}
		},
		onCancelEditDraft: function (oEvent) {
			var sDeliveryDate = oComponent_VendCr.getModel("JSON").getProperty("/items")[0].DelvDate || new Date();
			oComponent_VendCr.getModel("JSON").setProperty("/DraftFilters/DeliveryDays", '90');
			oComponent_VendCr.getModel("JSON").setProperty("/DraftFilters/deliveryDate", sDeliveryDate);
			oComponent_VendCr.getModel("JSON").setProperty("/DraftFilters/validDate" , true);
			this.onCloseDialog('', 'EditDraft');
		},
		onPressCart: function (oEvent) {

			var aItems = [],
				oDataModel = oComponent_VendCr.getModel("JSON").getData(),
				rows = oDataModel.items,
				oDeliveryDate = oDataModel.Filters.deliveryDate;
			oComponent_VendCr.getModel("JSON").setProperty("/cartClicked", true);
			for (var i = 0; i < rows.length; i++) {
				if (!!rows[i].Selected) {
					if (Number(rows[i].OrderQuan) === 0) {
						MessageToast.show(oComponent_VendCr.i18n('noCountMsg'), {
							duration: 1500
						});
						return;
					}
					else {
						rows[i].DelvDate = rows[i].DelvDate || oDeliveryDate || new Date();
						rows[i].Valid = true;
						aItems.push(rows[i]);
					}

				}
			}
			if (aItems.length < 1) {
				MessageToast.show(oComponent_VendCr.i18n('noSelectedItemsMsg'), {
					duration: 1500
				});
			}
			else {
				oComponent_VendCr.getModel("JSON").setProperty("/CartItems", jQuery.extend(true, [], aItems));
				oComponent_VendCr.getModel("JSON").setProperty("/oldCartItems", jQuery.extend(true, [], aItems));
				oComponent_VendCr.getModel("JSON").setProperty("/oldCartTotalPrice", oComponent_VendCr.getModel("JSON").getProperty("/TotalPrice"));
				oComponent_VendCr.getModel("JSON").setProperty("/cartTotalPrice", oComponent_VendCr.getModel("JSON").getProperty("/TotalPrice"));
				this.getPartners();
				this.getEmailsList();
				this.onOpenDialog('', 'Cart');
			}
		},
		getEmailsList: function (oEvent) {
			var sVendor = oComponent_VendCr.getModel("JSON").getProperty("/Filters/Vendor");
			models.getVendorCpEmails(sVendor).then((data) => {
				for (var i = 0; i < data.results.length; i++) {
					data.results[i].Selected = false;
				}
				oComponent_VendCr.getModel("JSON").setProperty("/emailsList", data.results);

			}).catch((error) => {
				models.handleErrors(error);
			});
		},
		getPartners: function (oEvent) {
			var sContract = oComponent_VendCr.getModel("JSON").getProperty("/CartItems/0/PurchaseAgreement");
			models.LoadPartners(sContract).then((data) => {
				oComponent_VendCr.getModel("JSON").setProperty("/partners", data);
			}).catch((error) => {
				models.handleErrors(error);
			});
		},
		removeDuplicates: function (array, key) {
			let lookup = new Set();
			return array.filter(obj => !lookup.has(obj[key]) && lookup.add(obj[key]));
		},

		onListItemPress: function (oEvent, sMaterial) {
			var oRow = oEvent.getSource().getBindingContext("JSON").getObject();
			var sOrderNum = oComponent_VendCr.getModel("JSON").getProperty("/OrderNumber") || '';
			oComponent_VendCr.getModel("JSON").setProperty("/SelectedItem", oRow);
			oComponent_VendCr.getModel("JSON").setProperty("/SelectedTab", 'MaterialDetails');
			models.LoadDetails(sMaterial).then((data) => {
				oComponent_VendCr.getModel("JSON").setProperty("/MatDetails", data);
			}).catch((error) => {
				models.handleErrors(error);
			});



			this.oRouter.navTo("detail", {
				Material: sMaterial,
				OrderNumber: sOrderNum
			});
		},
		beforeNavToSearchPage: function (oEvent) {
			MessageBox.warning("חישוב מחדש יאתחל את נתוני המסך", {
				actions: ['ביטול', 'אישור'],
				emphasizedAction: 'אישור',
				onClose: function (sAction) {
					if (sAction === 'אישור') {
						oComponent_VendCr._MasterController.NavToSearchPage();
					}

				}
			});

		},
		NavToSearchPage: function (oEvent) {
			this.oRouter.navTo("SearchPage");
		},
		onSearch: function (oEvent) {
			var sQuery = oEvent.getParameter("newValue");

			if (sQuery && sQuery.length > 0) {
				var oFilter = new Filter({
					filters: [new Filter('MatDescription', FilterOperator.Contains, sQuery),
					new Filter('Matnr', FilterOperator.Contains, sQuery)
					],
					and: false
				});
			}

			this.getView().byId("dataTable").getBinding("items").filter(oFilter, "Application");
		},
		filterItems: function (oEvent) {
			var aFilters = [];
			var model = oComponent_VendCr.getModel("JSON");
			var fData = model.getProperty("/Filters");
			var InventoryDays = fData.InventoryDaysSlider;
			if (InventoryDays[0] > InventoryDays[1]) {
				InventoryDays[0] = InventoryDays.splice(1, 1, InventoryDays[0])[0];
			}
			if (InventoryDays[1] === 10000) {
				InventoryDays[1] = 1000000;
			}
			aFilters.push(new sap.ui.model.Filter("InventoryDays", FilterOperator.BT, InventoryDays[0], InventoryDays[1]));
			if (fData.MainMatType) {
				aFilters.push(new sap.ui.model.Filter("MatType", "EQ", fData.MainMatType));
			}
			if (fData.Matnr && fData.Matnr.length) {
				for (const element of fData.Matnr) {
					aFilters.push(new sap.ui.model.Filter("Matnr", "EQ", element.Matnr));
				}
			}

			if (fData.AtcCode && fData.AtcCode.length) {
				for (const element of fData.AtcCode) {
					aFilters.push(new sap.ui.model.Filter("AtcCode", "EQ", element.AtcCode));
				}
			}
			if (fData.PurchasingGroup && fData.PurchasingGroup.length) {
				for (const element of fData.PurchasingGroup) {
					aFilters.push(new sap.ui.model.Filter("PurchasingGroup", "EQ", element.PurchasingGroup));
				}
			}
			if (fData.BlockedIndicator === '2') {
				aFilters.push(new sap.ui.model.Filter("BlockedIndicator", "EQ", false));
			}
			if (fData.BlockedIndicator === '3') {
				aFilters.push(new sap.ui.model.Filter("BlockedIndicator", "EQ", true));
			}
			if (fData.Item29 === '2') {
				aFilters.push(new sap.ui.model.Filter("Item29", "EQ", false));
			}
			if (fData.Item29 === '3') {
				aFilters.push(new sap.ui.model.Filter("Item29", "EQ", true));
			}
			if (fData.DrugsOnly === '2') {
				aFilters.push(new sap.ui.model.Filter("SupplyType", "NE", 'Z09'));
			}
			if (fData.DrugsOnly === '3') {
				aFilters.push(new sap.ui.model.Filter("SupplyType", "EQ", 'Z09'));
			}

			var oTable = oComponent_VendCr._MasterController.getView().byId("dataTable");
			var oBinding = oTable.getBinding("items");
			oBinding.filter(aFilters, "Application");
			var count = oTable.getBinding("items").getLength();
			oComponent_VendCr.getModel("JSON").setProperty("/ItemsCount", count);

		},
		clearFilters: function (oEvent) {
			var model = oComponent_VendCr.getModel("JSON"),
				modelData = model.getData();
			if (modelData.Filters.MatType === "") {
				model.setProperty("/Filters/MainMatType", '');
			}
			model.setProperty("/Filters/InventoryDaysSlider", [0, 10000]);
			model.setProperty("/Filters/Matnr", '');
			model.setProperty("/Filters/AtcCode", '');
			model.setProperty("/Filters/PurchasingGroup", '');
			model.setProperty("/Filters/Item29", '1');
			model.setProperty("/Filters/DrugsOnly", '1');
			model.setProperty("/Filters/BlockedIndicator", '2');
			oComponent_VendCr._MasterController.filterItems();
		},
		createColumnConfig: function () {
			var aCols = oComponent_VendCr._MasterController.byId("dataTable").getColumns();
			var aColumns = [];
			aColumns.push({
				label: oComponent_VendCr.i18n('Material'),
				property: 'Matnr'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('MaterialDesc'),
				property: 'MatDescription'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('TotalInventory'),
				property: ['AllCurrentStockUnit', 'AllCurrentStock'],
				template: '{0}, {1}'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('Issues'),
				property: ['IssuedStockUnit', 'IssuedStock'],
				template: '{0}, {1}'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('AverageMonthIssues'),
				property: ['IssuedStockUnit', 'IssueAvg'],
				template: '{0}, {1}'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('UnitBox'),
				property: 'UnitInBoxQty'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('RecommendedOrderQuantity'),
				property: ['RecommendedUnit', 'RecommendedQuan'],
				template: '{0}, {1}'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('OrderQuantity'),
				property: 'OrderQuan'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('unitPriceOrTotal'),
				property: ['NetPrice', 'ToltalPrice', 'Currency'],
				template: '{0} / {1}, {2}'
			});
			aColumns.push({
				label: oComponent_VendCr.i18n('InventoryDays'),
				property: 'InventoryDays'
			});


			return aColumns;

		},
		onExport: function (oEvent) {
			var aCols, aItems, oSettings, oSheet;
			aCols = this.createColumnConfig();
			aItems = oComponent_VendCr.getModel("JSON");
			var aSelectedModel = [];
			var aIndices = oComponent_VendCr._MasterController.byId("dataTable").getBinding("items").aIndices;
			var aSelectedModel = [];
			for (var i = 0; i < aIndices.length; i++) {
				aSelectedModel.push(aItems.getProperty("/items/" + aIndices[i]));
			}
			oSettings = {
				workbook: {
					columns: aCols
				},
				dataSource: aSelectedModel,
				fileName: "הזמנת רכש"
			};

			oSheet = new Spreadsheet(oSettings);
			oSheet.build()
				.then(function () { });

		},
		goToReport: function (oEvent) {
			var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
			var semamticActionObj = "zmm_vendor_po_rep-display";
			oCrossAppNavigator.isIntentSupported([semamticActionObj])
				.done((aResponses) => {
					if (aResponses[semamticActionObj].supported === true) {
						var hash = (oCrossAppNavigator && oCrossAppNavigator.hrefForExternal({
							target: {
								semanticObject: 'zmm_vendor_po_rep',
								action: 'display'
							}
						})) || "";
						sap.m.URLHelper.redirect(hash, true);
					}

				})
				.fail(function () { });
		}

	});
});