var oComponent_VendCr;
sap.ui.define([
	"sap/base/util/UriParameters",
	"sap/ui/core/UIComponent",
	"sap/ui/model/json/JSONModel",
	"sap/f/library",
	"sap/f/FlexibleColumnLayoutSemanticHelper",
	"zmm_vendor_po_cr/model/models"
], function (UriParameters, UIComponent, JSONModel, library, FlexibleColumnLayoutSemanticHelper, models) {
	"use strict";

	var LayoutType = library.LayoutType;

	var Component = UIComponent.extend("zmm_vendor_po_cr.Component", {
		metadata: {
			manifest: "json"
		},
		models: models,

		init: function () {
			
			UIComponent.prototype.init.apply(this, arguments);
			//Set Right to Left
			sap.ui.getCore().getConfiguration().setRTL(true);
			//Set HE language
			sap.ui.getCore().getConfiguration().setLanguage("iw_IL");
			oComponent_VendCr = this;
			this.getRouter().initialize();
			var oModel = new JSONModel();
			this.setModel(oModel);
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			// set the FLP model
			this.setModel(models.createFLPModel(), "FLP");
			//create json model
			this.setModel(models.createJSONModel(), "JSON");
			//busyIndicator
			this.getModel("ODATA").attachBatchRequestSent(function () {
				sap.ui.core.BusyIndicator.show();
			}).attachBatchRequestCompleted(function (event) {
				sap.ui.core.BusyIndicator.hide();
			});
			this.getModel("SH").attachBatchRequestSent(function () {
				sap.ui.core.BusyIndicator.show();
			}).attachBatchRequestCompleted(function (event) {
				sap.ui.core.BusyIndicator.hide();
			});

		},
		/**
		 * Returns an instance of the semantic helper
		 * @returns {sap.f.FlexibleColumnLayoutSemanticHelper} An instance of the semantic helper
		 */
		getHelper: function () {
			var oFCL = this.getRootControl().byId("fcl"),
				// oParams = UriParameters.fromQuery(location.search),
				oSettings = {
					defaultTwoColumnLayoutType: LayoutType.TwoColumnsMidExpanded,
					defaultThreeColumnLayoutType: LayoutType.ThreeColumnsMidExpandedEndHidden,
					mode: "ThreeColumnsMidExpandedEndHidden",
					initialColumnsCount: 3,
					maxColumnsCount: 3
				};

			return FlexibleColumnLayoutSemanticHelper.getInstanceFor(oFCL, oSettings);
		},
		i18n: function (str) {
			return oComponent_VendCr.getModel("i18n").getProperty(str);
		}
	});
	return Component;
});