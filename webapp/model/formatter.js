/*global true oComponent*/
sap.ui.define([], function() {
	"use strict";

	return {
		getPlantDesc: function(sKey,aPlants){
			if(aPlants && aPlants[sKey])
				return aPlants[sKey]['PlantDesc'];
			return "";
		},
		addComma: function(value) {
			if (value || value === 0) {
				value = parseFloat(value).toFixed(2);
				return value.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}
			return "";
		},
		
		getPlantSum: function(sKey,aPlants){
			if(aPlants && aPlants[sKey])
				return aPlants[sKey]['SumAfterVat'].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			return "";
		},
		getPlantCount: function(sKey,aPlants){
			if(aPlants && aPlants[sKey])
				return aPlants[sKey]['OrdersCount'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			return "0";
		},
		getMaterialIcon: function(sMatType){
			if (sMatType === 'ZPRP'){
				return 'sap-icon://pharmacy'
			}
			if(sMatType === 'ZSPN'){
				return 'sap-icon://stethoscope'
			}
		},
		setTableColumnsSum: function(aItems, sKey, sDigits) {
			if (aItems) {
				var sum = aItems.reduce((a, b) => a + (parseFloat(b[sKey]) || 0), 0); // sum method
				sum = sum.toFixed(parseInt(sDigits)); //Set digits - for example 3 in quantity case
				return sum.replace(/\B(?=(\d{3})+(?!\d))/g, ","); //regax whice adds comma to numbers
			}
		},
	};

});