/* global oComponent_VendCr : true */
/* global isAttachRequest : true */
/* global oModels : true */
/* global OWATracker: true */
/* global trackingPlugin: true */
sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device",
	"sap/ui/model/Filter",
	"sap/m/MessageBox"
], function (JSONModel, Device, Filter, MessageBox) {
	"use strict";

	return {
		createDeviceModel: function () {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createFLPModel: function () {
			var fnGetuser = jQuery.sap.getObject("sap.ushell.Container.getUser"),
				bIsShareInJamActive = fnGetuser ? fnGetuser().isJamActive() : false,
				oModel = new JSONModel({
					isShareInJamActive: bIsShareInJamActive
				});
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},

		createJSONModel: function () {
			var oModel = new sap.ui.model.json.JSONModel({
				TodayDate: new Date(),
				VendorDesc: "",
				deletedItems: [],
				TotalPrice: 0,
				cartClicked: false,
				CartCount: 0,
				SelectedTab: 'MaterialDetails',
				createDraftClicked: false,
				editCart: false,
				IssuesRegion: "M02",
				DatesError: false,
				genricMatnr: '',
				Filters: {
					DeliveryDays: '90',
					BeginDateTxt: String(new Date(new Date().setDate(new Date().getDate() - 90)).getDate()) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 90)).getMonth() + 1) + '.' + String(new Date(new Date().setDate(new Date().getDate() - 90)).getFullYear()),
					BeginDate: new Date(new Date().setDate(new Date().getDate() - 90)),
					EndDate: new Date(),
					orderType: "01",
					EndDateTxt: String(new Date().getDate()) + '.' + String(new Date().getMonth() + 1) + '.' + String(new Date().getFullYear()),
					OpenPoFlag: false,
					OpenPoDays: "90",
					DirectFlag: false,
					Matnr: [],
					Matnrs: [],
					MatType: '',
					BlockedIndicator: '2',
					Item29: '1',
					DrugsOnly: false,
					deliveryDate: new Date(),
					InventoryDaysSlider: [0,10000]
				},
				DraftFilters: {
					DeliveryDays: '90',
					deliveryDate: new Date(),
					validDate: true
				},
				layout: 'OneColumn',
				WaitingOrders: [],
				InvoicesList: [],
				VendorsList: [],
				newOrder: {
					PriceOutput: true,
					LgortFlag: false
				},
				SelectedAll: false,
				CardCount: 0,
				ItemsClicked: false,
				OrderData: {},
				DrugsItems:[{value:"ללא סינון" , key:'1'},
				{value:"ללא סמים" , key:'2'},
				{value:"סמים בלבד" , key:'3'}],
				BlockedIndicatorItems:[{value:"ללא סינון" , key:'1'},
				{value:"ללא חסומים" , key:'2'},
				{value:"חסומים בלבד" , key:'3'}],
				Item29Items: [{value:"ללא סינון" , key:'1'},
				{value:"ללא פרטי 29ג'" , key:'2'},
				{value:"פרטי 29ג' בלבד" , key:'3'}],
				IdsList: [],
				VouchersList: [],
				oPlantsData: {},
				AllPlantsCounter: 0
			});
			oModel.setSizeLimit(1000);
			return oModel;
		},
		LoadItems: function (oEntry) {
			return new Promise(function (resolve, reject) {
				oComponent_VendCr.getModel("ODATA").create("/MaterialDataNewSet", oEntry, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);

						console.log(error);

					}
				});
			});
		},
		// LoadItems: function (sOrderNum) {
		// 	var oModelData = oComponent_VendCr.getModel("JSON").getData().Filters;
		// 	const aFilters = [
		// 		new sap.ui.model.Filter("IvDeliveryDays", "EQ", oModelData.DeliveryDays),
		// 		new sap.ui.model.Filter("IvMatType", "EQ", oModelData.MatType || ''),
		// 		new sap.ui.model.Filter("IvIssueFrom", "EQ", this.getFixedDate(oModelData.BeginDate)),
		// 		new sap.ui.model.Filter("IvIssueTo", "EQ", this.getFixedDate(oModelData.EndDate)),
		// 		new sap.ui.model.Filter("IvOpenPoInd", "EQ", oModelData.OpenPoFlag),
		// 		new sap.ui.model.Filter("IvOpenPoDays", "EQ", oModelData.OpenPoDays || '30'),
		// 		new sap.ui.model.Filter("IvOrderNum", "EQ", sOrderNum || ''),
		// 		new sap.ui.model.Filter("IvVendor", "EQ", oModelData.Vendor || '')

		// 	];
			
		// 	return new Promise(function (resolve, reject) {
		// 		oComponent_VendCr.getModel("ODATA").read("/MaterialDataSet", {
		// 			filters: aFilters,
		// 			success: function (data) {
		// 				resolve(data);
		// 			},
		// 			error: function (error) {
		// 				reject(error);

		// 				console.log(error);

		// 			}
		// 		});
		// 	});
		// },
		LoadDetails: function (sMaterial) {
			const sKey = oComponent_VendCr.getModel("ODATA").createKey("/ItemDetailsSet", {
				IvMatnr: sMaterial
			});
			return new Promise(function (resolve, reject) {
				oComponent_VendCr.getModel("ODATA").read(sKey, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);

						console.log(error);

					}
				});
			});
		},
		LoadPartners: function (sContract) {
			const sKey = oComponent_VendCr.getModel("ODATA").createKey("/PartnersOfVendorSet", {
				IvContract: sContract
			});
			return new Promise(function (resolve, reject) {
				oComponent_VendCr.getModel("ODATA").read(sKey, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);

						console.log(error);

					}
				});
			});
		},
		createOrder: function (oEntry) {
			return new Promise(function (resolve, reject) {
				oComponent_VendCr.getModel("ODATA").create("/OrderHeaderSet", oEntry, {
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);

						console.log(error);

					}
				});
			});

		},

		getVendorCpEmails: function (sVendor) {
			const aFilters = [
				new sap.ui.model.Filter("IvVendor", "EQ", sVendor)
			];
			return new Promise(function (resolve, reject) {
				oComponent_VendCr.getModel("ODATA").read("/VendorCpEmailsSet", {
					filters: aFilters,
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);

						console.log(error);

					}
				});
			});
		},
		LoadGenericsMaterials: function (sMatnr) {
			var oModelData = oComponent_VendCr.getModel("JSON").getData().Filters;
			const aFilters = [
				new sap.ui.model.Filter("IvDeliveryDays", "EQ", oModelData.DeliveryDays),
				new sap.ui.model.Filter("IvMatnr", "EQ", sMatnr),
				new sap.ui.model.Filter("IvMatType", "EQ", oModelData.MatType || ''),
				new sap.ui.model.Filter("IvIssueFrom", "EQ", this.getFixedDate(oModelData.BeginDate)),
				new sap.ui.model.Filter("IvIssueTo", "EQ", this.getFixedDate(oModelData.EndDate)),
				new sap.ui.model.Filter("IvOpenPoInd", "EQ", oModelData.OpenPoFlag),
				new sap.ui.model.Filter("IvOpenPoDays", "EQ", oModelData.OpenPoDays || '30')

			];
			return new Promise(function (resolve, reject) {
				oComponent_VendCr.getModel("ODATA").read("/AtcDetailsSet", {
					filters: aFilters,
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);

						console.log(error);

					}
				});
			});
		},
		LoadIssues: function (sMatnr) {
			var oModelData = oComponent_VendCr.getModel("JSON").getData().Filters;
			var oModel = oComponent_VendCr.getModel("ODATA");
			const sKey = oModel.createKey("/ItemIssueHDRSet", {
				IvIssueFrom: this.getFixedDate(oModelData.BeginDate),
				IvIssueTo: this.getFixedDate(oModelData.EndDate),
				IvMatnr: sMatnr
			});
			return new Promise((resolve, reject) => {
				oModel.read(sKey, {
					urlParameters: {
						"$expand": "ItemIssueLn"
					},
					success: (data) => {
						resolve(data);
					},
					error: (error) => {
						reject(error);
					}
				});
			});
		},
		LoadExcel: function (sEbeln) {
			const aFilters = [
				new sap.ui.model.Filter("IvEbeln", "EQ", sEbeln)
			];
			return new Promise(function (resolve, reject) {
				oComponent_VendCr.getModel("ODATA").read("/ExcelDataSet", {
					filters: aFilters,
					success: function (data) {
						resolve(data);
					},
					error: function (error) {
						reject(error);
						console.log(error);

					}
				});
			});
		},
		LoadOrderHistory: function (sMatnr, sVendor) {
			var oModel = oComponent_VendCr.getModel("ODATA");
			const sKey = oModel.createKey("/ItmPoHstHDRSet", {
				IvMatnr: sMatnr,
				IvVendor: sVendor

			});
			return new Promise((resolve, reject) => {
				oModel.read(sKey, {
					urlParameters: {
						"$expand": "ItmPoHstGRSet,ItmPoHstLnSet"
					},
					success: (data) => {
						resolve(data);
					},
					error: (error) => {
						reject(error);
					}
				});
			});
		},
		handleErrors: function (error) {
			try {
				MessageBox.error(JSON.parse(error.responseText).error.message.value);
			} catch (e) {
				MessageBox.error(JSON.stringify(error.message));
			}
		},
		getFixedDate: function (value) {
			if (!value) {
				return;
			}
			var before = value.getTime();
			var after = new Date(before + 10800000); //adding three hours to fix UTC gateway processing day back
			return after;
		}
	};

});